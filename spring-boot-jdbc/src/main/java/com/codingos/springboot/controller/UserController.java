package com.codingos.springboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codingos.springboot.entity.ResponseData;
import com.codingos.springboot.entity.User;
import com.codingos.springboot.service.UserService;

@RestController
public class UserController {
	
	@Autowired
	private UserService userService;

	@PostMapping("/addUser")
	public User addUser(@RequestParam String name, @RequestParam Integer age) {
		userService.addUser(name, age);
		User user = new User();
		user.setName(name);
		user.setAge(age);
		return user;
	}
	
	@DeleteMapping("/deleteUser")
	public ResponseData deleteUser(Integer id) {
		ResponseData responseData = new ResponseData();
		int result = userService.deleteUserById(id);
		responseData.setData(result);
		return responseData;
	}
	
	@PutMapping("/updateUser")
	public ResponseData updateUser(@RequestBody User user) {
		ResponseData responseData = new ResponseData();
		userService.updateUser(user);
		return responseData;
	}
	
	@GetMapping("/queryUser")
	public ResponseData queryUser(Integer id) {
		ResponseData responseData = new ResponseData();
		userService.queryUserById(id);
		return responseData;
	}
}
