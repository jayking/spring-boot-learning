package com.codingos.springboot.service;

import com.codingos.springboot.entity.User;

public interface UserService {

	public void addUser(String name,Integer age);
	
	public Integer deleteUserById(Integer id);
	
	public void updateUser(User user);
	
	public User queryUserById(Integer id);
}
