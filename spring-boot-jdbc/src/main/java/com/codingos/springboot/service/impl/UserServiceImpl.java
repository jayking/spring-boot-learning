package com.codingos.springboot.service.impl;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Service;

import com.codingos.springboot.entity.User;
import com.codingos.springboot.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Override
	public void addUser(String name, Integer age) {
		// 数据库user表的主键id已经设置为自增
		int ss = jdbcTemplate.update("insert into user (name,age) values (?,?)", name, age);
		// 返回的是执行sql后影响的数据库记录条数
		System.out.println(ss);
	}

	@Override
	public Integer deleteUserById(Integer id) {
		int result = jdbcTemplate.update("delete from user where id = ?", id);
		return result;
	}

	@Override
	public void updateUser(User user) {
		String updateSql = "update user set name = :name, age = :age where id = :id";
		SqlParameterSource sqlParameterSource = new BeanPropertySqlParameterSource(user);
		int i = namedParameterJdbcTemplate.update(updateSql, sqlParameterSource);
		System.out.println(i);
	}

	@Override
	public User queryUserById(Integer id) {
		List<String> name = jdbcTemplate.queryForList("select name from user where id = ?", new Object[] {id}, String.class);
		// 实验代码
		List<User> userList = jdbcTemplate.query("select * from user", new RowMapper<User>() {
			@Override
			public User mapRow(ResultSet rs, int rowNum) throws SQLException {
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setName(rs.getString("name"));
				user.setAge(rs.getInt("age"));
				return user;
			}
		});
		
		User user = jdbcTemplate.queryForObject("select * from user where id = ?", new Object[] {id}, new RowMapper<User>() {
			@Override
			public User mapRow(ResultSet rs, int rowNum) throws SQLException {
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setName(rs.getString("name"));
				user.setAge(rs.getInt("age"));
				return user;
			}
		});
		return user;
	}
}
