package com.codingos.springboot;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.codingos.springboot.service.UserService;


@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootJdbcApplicationTests {
	
	@Autowired
	private UserService userService;

	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testAddUser() {
		userService.addUser("zhangsan", 13);
	}

}
