package com.codingos.springboot;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
//	private Logger logger = LoggerFactory.getLogger(Controller.class);
	private Logger logger = LogManager.getLogger(Controller.class);
	
	
	@GetMapping("testlog")
	public void testlog() {
		
		logger.info("**************** log info ******************");
		logger.debug("**************** log debug ******************");
		logger.error("**************** log error ******************");
		logger.warn("**************** log warn ******************");
	}
}
