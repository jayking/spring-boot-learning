package com.codingos.springboot.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Aspect
@Component
public class HttpAspect {
	private final static Logger logger = LoggerFactory.getLogger(HttpAspect.class);

	@Pointcut("execution(public * com.codingos.springboot.controller.UserController.*(..))")
	public void log() {
	}

	/**
	 *  在被切入的方法执行之前执行
	 */
	@Before("log()")
	public void doBefore(JoinPoint joinPoint) {
		logger.info("***************** before aop 执行 ****************");
		ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		// url
		logger.info("url={}", attributes.getRequest().getRequestURL());
		// methond
		logger.info("method={}", attributes.getRequest().getMethod());
		// ip
		logger.info("ip={}", attributes.getRequest().getRemoteAddr());
		// 类方法
		logger.info("class_method={}", joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
		// 参数
		logger.info("args={}", joinPoint.getArgs());
	}

	/**
	 *  在被切入的方法执行之后执行
	 */
	@After("log()")
	public void doAfter() {
		logger.info("************************ after aop 执行 **************************");
	}
	
	/**
	 * 用来获取返回值
	 */
	@AfterReturning(returning = "object",pointcut = "log()")
	public void doAfterReturning(Object object) {
		logger.info("response={}", object.toString());
	}
	
	
	
	
	
	
	
	
//	// 在被切入的方法执行之前执行
//	@Before("execution(public * com.codingos.springboot.controller.UserController.*(..))")
//	public void doBefore() {
//		System.out.println("***************** before aop 执行 ****************");
//	}
//
//	// 在被切入的方法执行之后执行
//	@After("execution(public * com.codingos.springboot.controller.UserController.*(..))")
//	public void doAfter() {
//		System.out.println("************************ after aop 执行 **************************");
//	}
}
