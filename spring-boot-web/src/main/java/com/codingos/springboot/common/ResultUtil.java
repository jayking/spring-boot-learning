package com.codingos.springboot.common;

public class ResultUtil<T> {

	public static <T> Result<T> success(T object) {
		Result<T> result = new Result<>();
		result.setCode(0);
		result.setMessage("成功");
		result.setData(object);
		return result;
	}
	
	public static <T> Result<T> success() {
		Result<T> result = new Result<>();
		result.setCode(0);
		result.setMessage("成功");
		return result;
	}
	
	public static <T> Result<T> error(Integer code, String message) {
		Result<T> result = new Result<>();
		result.setCode(code);
		result.setMessage(message);
		return result;
	}
}
