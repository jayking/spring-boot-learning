package com.codingos.springboot.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codingos.springboot.common.Result;
import com.codingos.springboot.common.ResultUtil;
import com.codingos.springboot.entity.User;

@RestController
public class UserController {
	private final static Logger logger = LoggerFactory.getLogger(UserController.class);

	@PostMapping("/addUser")
	public Result<User> addUser(@Valid User user, BindingResult bindingResult) throws Exception {
		logger.info("*************** addUser() ***************");
		Result<User> result = new Result<>();
		if (bindingResult.hasErrors()) {
			// 打印出字段验证信息
			logger.error(bindingResult.getFieldError().getDefaultMessage());
//			result = ResultUtil.error(1, bindingResult.getFieldError().getDefaultMessage());
			throw new Exception(bindingResult.getFieldError().getDefaultMessage());
		} else {
			result = ResultUtil.success(user);
		}

		return result;
	}

	@GetMapping("getUser")
	public Result<User> getUser(Integer id) {
		logger.info("****************** getUser() ******************");
		User user = new User();
		user.setId(1);
		user.setName("zhangsan");
		user.setAge(11);
		
		return ResultUtil.success(user);
	}
}
