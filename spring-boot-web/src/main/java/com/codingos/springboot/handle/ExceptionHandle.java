package com.codingos.springboot.handle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.codingos.springboot.common.Result;
import com.codingos.springboot.common.ResultUtil;

@ControllerAdvice
@RestControllerAdvice
public class ExceptionHandle {
	private static Logger logger = LoggerFactory.getLogger(ExceptionHandle.class);

	@ExceptionHandler(value=Exception.class)
	public <T> Result<T> handle(Exception e) {
		logger.error(e.getMessage(),e);
		return ResultUtil.error(1, e.getLocalizedMessage());
	}
}
